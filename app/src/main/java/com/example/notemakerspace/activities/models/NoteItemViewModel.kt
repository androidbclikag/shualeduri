package com.example.notemakerspace.activities.models

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.notemakerspace.activities.room.User


class NoteItemViewModel : ViewModel() {

    private val liveUser: MutableLiveData<User> by lazy {
        MutableLiveData<User>()
    }

    fun noteSet(user: User) {
        liveUser.postValue(user)
    }

    fun noteGet(): MutableLiveData<User> {
        return liveUser
    }


}