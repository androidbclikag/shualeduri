package com.example.notemakerspace.activities.ui.fragments.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.notemakerspace.R
import com.example.notemakerspace.activities.models.databind.ColorModel

class ColorModelRecyclerViewAdapter(
    private val dataSet: ArrayList<ColorModel.MyColorModel>,
    private val onClick: CustomOnClick
) : RecyclerView.Adapter<ColorModelRecyclerViewAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        fun onBind() {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            onClick.onClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.color_button_layout, parent, false)
    )

    override fun getItemCount(): Int = dataSet.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}