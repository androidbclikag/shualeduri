package com.example.notemakerspace.activities.sharedpreferences

import android.content.Context
import android.content.SharedPreferences

class UserData {
    companion object {
        private var userData: UserData? = null
        fun instance(): UserData {
            if (userData == null)
                userData = UserData()
            return userData!!
        }
    }

    private val sharedPreferences: SharedPreferences by lazy {
        App.instance!!.context!!.getSharedPreferences("UserData", Context.MODE_PRIVATE)
    }

    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }

    fun saveData(key: String, value: String) {
        editor.putString(key, value)
        editor.apply()

    }

    fun readData(key: String) = sharedPreferences.getString(key, "")
}