package com.example.notemakerspace.activities.ui.fragments.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.util.Log.w
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.notemakerspace.R
import com.example.notemakerspace.activities.ui.fragments.activities.AuthorizationRegistrationActivity
import com.example.notemakerspace.activities.ui.fragments.activities.WorkingDashboard
import com.example.notemakerspace.activities.sharedpreferences.UserData
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_sign_in.view.*


class SignInFragment : Fragment() {

    private lateinit var auth: FirebaseAuth
    private lateinit var itemView: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemView = inflater.inflate(R.layout.fragment_sign_in, container, false)
        readUserData()
        init()
        return itemView
    }

    private fun init() {

        itemView.signInButton.setOnClickListener { signIn() }

    }

    private fun signIn() {

        val email = itemView.emailEditTextSI.text.toString()
        val password = itemView.passwordEditTextSI.text.toString()
        val activityContext = activity as AuthorizationRegistrationActivity

        if (email.isNotEmpty() && password.isNotEmpty()) {
            auth = FirebaseAuth.getInstance()
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activityContext) { task ->
                    if (task.isSuccessful) {
                        saveUserData()
                        val intent = Intent(
                            activityContext,
                            WorkingDashboard::class.java
                        )
                        startActivity(intent)
                        activityContext.finish()
                        d("success", "signInWithEmail:success")
                    } else {
                        w("", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            activityContext, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
        } else {
            Toast.makeText(
                activityContext, "Please fill all fields",
                Toast.LENGTH_SHORT
            ).show()
        }
    }


    private fun saveUserData() {
        UserData.instance().saveData("UserEmail", itemView.emailEditTextSI.text.toString())
        UserData.instance().saveData("UserPassword", itemView.passwordEditTextSI.text.toString())

    }


    private fun readUserData() {
        val email = UserData.instance().readData("UserEmail")
        val password = UserData.instance().readData("UserPassword")
        itemView.emailEditTextSI.setText(email)
        itemView.passwordEditTextSI.setText(password)


    }
}
