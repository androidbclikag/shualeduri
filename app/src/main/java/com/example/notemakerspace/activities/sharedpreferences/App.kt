package com.example.notemakerspace.activities.sharedpreferences

import android.app.Application
import android.content.Context

class App : Application() {
    var context: Context? = null

    companion object {
        var instance: App? = null
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
    }
}