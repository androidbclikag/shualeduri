package com.example.notemakerspace.activities.ui.fragments.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.notemakerspace.R
import com.example.notemakerspace.activities.room.User
import com.example.notemakerspace.databinding.NoteItemLayoutBinding
import kotlinx.android.synthetic.main.note_item_layout.view.*

class NotesRecyclerViewAdapter(
    private val dataSet: MutableList<User>,
    private val changeColor: CustomOnClick
) :
    RecyclerView.Adapter<NotesRecyclerViewAdapter.ViewHolder>() {
    inner class ViewHolder(private val binding: NoteItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun onBind() {
            binding.itemModel = dataSet[adapterPosition]
            binding.root.editColor.setOnClickListener(this)

        }


        override fun onClick(v: View?) {
            changeColor.onClick(adapterPosition)

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.note_item_layout,
            parent,
            false
        )
    )

    override fun getItemCount(): Int = dataSet.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.onBind()
}