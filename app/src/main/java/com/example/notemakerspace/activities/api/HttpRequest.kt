package com.example.notemakerspace.activities.api

import android.telecom.Call
import android.util.Log.d
import org.json.JSONObject
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


object HttpRequest {
    const val UNKNOWN = "unknown"


    const val HTTP_STATUS_CODE_OK_200 = 200
    const val HTTP_STATUS_CODE_CREATED_201 = 201
    const val HTTP_BAD_REQUEST_400 = 400
    const val HTTP_UNAUTHORIZED_401 = 401

    var retrofit: Retrofit =
        Retrofit.Builder().addConverterFactory(ScalarsConverterFactory.create())
            .baseUrl("https://reqres.in/api/")
            .build()


    private var service = retrofit.create(
        ApiService::class.java
    )


    private fun onCallBack(callBack: CustomCallBack) = object : Callback<String> {
        override fun onFailure(call: retrofit2.Call<String>, t: Throwable) {
            d("fail", "${t.message}")
            callBack.onFailure(t.message.toString())

        }

        override fun onResponse(call: retrofit2.Call<String>, response: Response<String>) {

            val statusCode = response.code()
            if (statusCode == HTTP_STATUS_CODE_CREATED_201 || statusCode == HTTP_STATUS_CODE_OK_200) {
                callBack.onResponse(response.body().toString())
            } else if (statusCode == HTTP_BAD_REQUEST_400 || statusCode == HTTP_UNAUTHORIZED_401) {

                val json = JSONObject(response.errorBody().toString())
                if (json.has("error"))
                    callBack.onFailure(json.getString("error"))
                d("error", "${response.errorBody()}")
                callBack.onError(response.errorBody().toString(), json.getString("error"))

            }


        }
    }


    fun getRequest(path: String, callBack: CustomCallBack) {
        val request = service.getRequest(path)
        request.enqueue(onCallBack(callBack))
    }


    interface ApiService {
        @GET("{path}")
        fun getRequest(@Path("path") path: String): retrofit2.Call<String>
    }
}