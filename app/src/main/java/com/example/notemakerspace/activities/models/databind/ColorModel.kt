package com.example.notemakerspace.activities.models.databind

import com.google.gson.annotations.SerializedName

class ColorModel {
    var data = ArrayList<MyColorModel>()

    class MyColorModel {
        var id = 0
        var name = ""
        var year = 0
        var color = ""

        @SerializedName("pantone_value")
        var pantoneValue = ""
    }

}