package com.example.notemakerspace.activities.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
    @ColumnInfo(name = "first_name") var title: String? = "",
    @ColumnInfo(name = "last_name") var description: String? = ""
)

