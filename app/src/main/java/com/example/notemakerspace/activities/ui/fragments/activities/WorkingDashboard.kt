package com.example.notemakerspace.activities.ui.fragments.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.room.Room
import com.example.notemakerspace.R
import com.example.notemakerspace.activities.ui.fragments.adapters.CustomOnClick
import com.example.notemakerspace.activities.ui.fragments.adapters.NotesRecyclerViewAdapter
import com.example.notemakerspace.activities.ui.fragments.fragments.NoteFragment
import com.example.notemakerspace.activities.models.databind.ColorModel
import com.example.notemakerspace.activities.room.AppDatabase
import com.example.notemakerspace.activities.room.User
import kotlinx.android.synthetic.main.activity_working_dashboard.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

class WorkingDashboard : AppCompatActivity() {
    companion object {
        const val REQUEST_CODE = 20
    }

    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()

    }
    var items: MutableList<User> = mutableListOf()
    private val colorItems = ArrayList<ColorModel.MyColorModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_working_dashboard)

        init()

    }


    private fun init() {
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.adapter =
            NotesRecyclerViewAdapter(
                items,
                object :
                    CustomOnClick {
                    override fun onClick(position: Int) {
                        val intent = Intent(this@WorkingDashboard, EditNoteActivity::class.java)
                        intent.putExtra("itemPosition", position)
                        startActivity(intent)

                    }
                })
        CoroutineScope(Main).launch {
            read()
            recyclerView.adapter!!.notifyDataSetChanged()
        }
        refresh()

    }


    private suspend fun read() {

        val users = db.userDao().getAll()
        for (user in users)
            items.add(user)
        recyclerView.adapter!!.notifyDataSetChanged()

    }


    private fun refresh() {
        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = true
            clearRecyclerview()
            Handler().postDelayed({
                swipeRefresh.isRefreshing = false
                CoroutineScope(Main).launch { read() }

            }, 2000)

        }
    }


    private fun clearRecyclerview() {
        items.clear()
    }


    fun expand(view: View) {
        addFragment(NoteFragment(), R.id.fragment, "noteFragment")
    }

    private fun addFragment(fragment: Fragment, container: Int, tag: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(container, fragment, tag)
        transaction.addToBackStack(tag)
        transaction.commit()

    }


}
