package com.example.notemakerspace.activities.ui.fragments.adapters

interface CustomOnClick {
    fun onClick(position: Int)
}