package com.example.notemakerspace.activities.ui.fragments.fragments

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room

import com.example.notemakerspace.R
import com.example.notemakerspace.activities.ui.fragments.activities.WorkingDashboard
import com.example.notemakerspace.activities.models.NoteItemViewModel
import com.example.notemakerspace.activities.room.AppDatabase
import com.example.notemakerspace.activities.room.User
import kotlinx.android.synthetic.main.activity_working_dashboard.*
import kotlinx.android.synthetic.main.fragment_note.view.*
import kotlinx.android.synthetic.main.fragment_note.view.saveButton
import kotlinx.android.synthetic.main.fragment_note.view.titleEditText
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch


class NoteFragment : BaseFragment() {
    private val db by lazy {
        Room.databaseBuilder(
            rootView!!.context,
            AppDatabase::class.java, "database-name"
        ).build()

    }

    private lateinit var noteItemViewModel: NoteItemViewModel
    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {
        rootView!!.saveButton.setOnClickListener {
            CoroutineScope(Main).launch { save() }
        }

        val liveItem = ViewModelProvider(this).get(NoteItemViewModel::class.java)
        liveItem.noteGet().observe(this, Observer {
            val note = it
            d("liveUser?", "$note")
            if (note != null) {
                val activity = activity as WorkingDashboard
                activity.items.add(0, note)
                activity.recyclerView.scrollToPosition(0)
                activity.recyclerView.adapter!!.notifyItemInserted(0)

                d("liveUser", "$note")

            }
        })


    }

    override fun getLayoutResource(): Int = R.layout.fragment_note


    private suspend fun save() {

        val titleEditText = rootView!!.titleEditText.text
        val descriptionEditText = rootView!!.descriptionEditText.text
        if (titleEditText.isNotEmpty() && descriptionEditText.isNotEmpty()) {
            val item = User()
            item.title = titleEditText.toString()
            item.description = descriptionEditText.toString()
            d("model", "$item.title")
            db.userDao().insertAll(item)
            for (i in db.userDao().getAll())
                d("logNote", "${i.title}")
            noteItemViewModel = ViewModelProvider(this).get(NoteItemViewModel::class.java)
            noteItemViewModel.noteSet(item)


        }


    }


}



