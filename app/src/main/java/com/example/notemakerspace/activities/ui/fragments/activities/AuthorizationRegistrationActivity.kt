package com.example.notemakerspace.activities.ui.fragments.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.notemakerspace.R
import com.example.notemakerspace.activities.ui.fragments.fragments.SignInFragment
import com.example.notemakerspace.activities.ui.fragments.fragments.SignUpFragment
import kotlinx.android.synthetic.main.activity_authorization_registration.*

class AuthorizationRegistrationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authorization_registration)
        setSignInFragment()
        init()
    }


    private fun init() {
        signUpButtonSuggestion.setOnClickListener {
            setSignUpFragment()
            signUpButtonSuggestion.visibility = View.GONE
            questionTextView.visibility = View.GONE
            welcomeTextView.visibility = View.GONE
            messageTextView.visibility = View.GONE


        }


    }


    private fun setSignUpFragment() {
        addFragment(SignUpFragment(), R.id.signUpInFragment, "signInFragment")
    }


    private fun setSignInFragment() {
        addFragment(SignInFragment(), R.id.signUpInFragment, "signUpFragment")


    }

    private fun addFragment(fragment: Fragment, container: Int, tag: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(container, fragment, tag)
        transaction.addToBackStack(tag)
        transaction.commit()

    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
            welcomeTextView.visibility = View.VISIBLE
            messageTextView.visibility = View.VISIBLE
            signUpButtonSuggestion.visibility = View.VISIBLE
            questionTextView.visibility = View.VISIBLE
        } else super.onBackPressed()
    }


}
