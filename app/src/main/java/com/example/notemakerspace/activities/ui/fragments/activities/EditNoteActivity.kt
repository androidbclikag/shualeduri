package com.example.notemakerspace.activities.ui.fragments.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.notemakerspace.R
import com.example.notemakerspace.activities.ui.fragments.adapters.CustomOnClick
import com.example.notemakerspace.activities.ui.fragments.adapters.ColorModelRecyclerViewAdapter
import com.example.notemakerspace.activities.api.CustomCallBack
import com.example.notemakerspace.activities.api.HttpRequest
import com.example.notemakerspace.activities.models.databind.ColorModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_edit_note.*
import kotlinx.android.synthetic.main.activity_working_dashboard.*

class EditNoteActivity : AppCompatActivity() {
    private var items = ArrayList<ColorModel.MyColorModel>()

    companion object {
        const val REQUEST_CODE = 21
    }

    private lateinit var adapter: ColorModelRecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_note)
        init()
    }

    private fun init() {
        getColor()

        editNoteRecyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter =
            ColorModelRecyclerViewAdapter(
                items,
                object :
                    CustomOnClick {
                    override fun onClick(position: Int) {
                        intent.getIntExtra("itemPosition", 0)

                    }

                })
        recyclerView.adapter = adapter

    }

    private fun getColor() {
        HttpRequest.getRequest(HttpRequest.UNKNOWN, object : CustomCallBack {
            override fun onResponse(response: String) {
                val colorModel = Gson().fromJson(response, ColorModel::class.java)
                val listOfModels = colorModel.data
                for (index in 0 until listOfModels.size) {
                    val data = listOfModels[index]
                    val model = ColorModel.MyColorModel()
                    model.color = data.color
                    model.id = data.id
                    model.name = data.name
                    model.year = data.year
                    model.pantoneValue = model.pantoneValue
                    if (items.size > 0)
                        items.add(model)
                    recyclerView.adapter!!.notifyDataSetChanged()

                }
            }
        })
    }


}
