package com.example.notemakerspace.activities.api

interface CustomCallBack {
    fun onResponse(response: String) {}

    fun onFailure(body: String) {}

    fun onError(body: String, message: String) {}

}