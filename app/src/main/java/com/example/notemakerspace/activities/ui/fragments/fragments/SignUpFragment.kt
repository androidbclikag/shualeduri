package com.example.notemakerspace.activities.ui.fragments.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast

import com.example.notemakerspace.R
import com.example.notemakerspace.activities.ui.fragments.activities.AuthorizationRegistrationActivity
import com.example.notemakerspace.activities.ui.fragments.activities.WorkingDashboard
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_sign_up.view.*


class SignUpFragment : BaseAuthFragment() {
    private lateinit var auth: FirebaseAuth
    override fun start(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) {

        init()

    }

    private fun init() {


        rootView!!.signUpButton.setOnClickListener {
            signUp()
        }

    }


    override fun getLayoutResource(): Int = R.layout.fragment_sign_up


    private fun signUp() {
        auth = FirebaseAuth.getInstance()
        val password = rootView!!.passwordEditText.text.toString()
        val confirmPassword = rootView!!.confirmPasswordEditText.text.toString()
        val email = rootView!!.emailEditText.text.toString()
        val context = activity as AuthorizationRegistrationActivity

        if (password == confirmPassword && password.isNotEmpty() && confirmPassword.isNotEmpty() && email.isNotEmpty()) {
            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(context) { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(
                            context,
                            WorkingDashboard::class.java
                        )
                        startActivity(intent)
                        context.finish()
                        Log.d("", "createUserWithEmail:success")
                    } else {
                        Log.w("", "createUserWithEmail:failure", task.exception)
                        Toast.makeText(
                            context, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()

                    }

                }
        } else {
            Toast.makeText(
                context, "Please fill all fields",
                Toast.LENGTH_SHORT
            ).show()
        }

    }

}
