package com.example.notemakerspace.activities.models.databind

import android.graphics.Color
import android.view.View
import androidx.databinding.BindingAdapter

object DataBindingComponents {

    @BindingAdapter("setColor")
    fun setColor(view: View, color: ColorModel.MyColorModel) {
        if (color.color.isNotEmpty()) {
            view.setBackgroundColor(Color.parseColor(color.color))
        }
    }
}