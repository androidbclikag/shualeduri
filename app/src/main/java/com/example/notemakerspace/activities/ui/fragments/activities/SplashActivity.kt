package com.example.notemakerspace.activities.ui.fragments.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.notemakerspace.R

class SplashActivity : AppCompatActivity() {
    companion object {
        const val SPLASH_TIME_OUT_LONG: Long = 3000
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)
    }

    override fun onStart() {
        super.onStart()
        Handler().postDelayed(
            { startMainActivity() },
            SPLASH_TIME_OUT_LONG
        )
    }

    override fun onPause() {
        super.onPause()
        Handler().removeCallbacks(
            { startMainActivity() },
            SPLASH_TIME_OUT_LONG
        )
    }


    private fun startMainActivity() {
        val intent = Intent(this, AuthorizationRegistrationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        finish()
    }

}
